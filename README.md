# Silly VM

Silly VM is a very simple virtual machine for embedding in Python applications.

It is written in C, bridged to Python via Cython, offers around 10x slowdown compared to
binary gcc-generated code.

## Installation

```bash
pip install git+https://gitlab.com/mborisyak/silly-vm.git
```

## Basics

Silly VM is an assembler VM, it executes a list of instructions one-by-one, relying on `jump`s
for implementing control flow. Each instruction is represented by a tuple
`(operation code, argument 1, argument 2)`, each argument is an integer (`int` in C).
Available operations are similar to a typical assembler language and quite self explanatory.
For example:
```python
code = """
  assign 0 21
  mul 0 2
"""

from sillyvm import compile_assembler, VirtualMachine

byte_code, _, _ = compile_assembler(code)

vm = VirtualMachine(byte_code, memory_size=1)
exit_status = vm()
print(vm.memory_dump())
```
outputs `array('i', [42])`.