from .grammar import *

from ..vm import Typing, OPERATION_GROUPS, OPCODES_BY_SIGNATURE

import array

__all__ = [
  'asm'
]

def type_constant(value, modifier):
  if modifier == '':
    return value, Typing.CONST
  elif modifier == '*':
    return value, Typing.VARIABLE
  elif modifier == '&':
    raise Exception('Constant does not have address!')
  else:
    raise Exception('Unknown modifier!')

def type_variable(value, modifier):
  if modifier == '':
    return value, Typing.VARIABLE
  elif modifier == '*':
    return value, Typing.POINTER
  elif modifier == '&':
    return value, Typing.CONST
  else:
    raise Exception('Unknown modifier!')

class Executable(object):
  def __init__(self, memory_limit=None):
    self.constants = dict()
    self.variables = dict()

    self.memory_limit = memory_limit
    self.allocated = 0

    self.memory = array.array('i')

  def add_constant(self, const_name, const_value):
    if const_name in self.constants:
      raise Exception('Attempting to redefine constant %s.' % (const_name,))

    if const_name in self.constants:
      raise Exception('Attempting to redefine variable %s' % (const_name, ))

    self.constants[const_name] = const_value

  def add_label(self, label_name):
    self.add_constant(label_name, self.allocated)

  def add_variable(self, var_name, var_value, size=1):
    size = size if size is not None else 1

    if size < 1:
      return

    if var_name in self.constants:
      raise Exception('Attempting to redefine constant %s' % (var_name,))

    if var_name in self.constants:
      raise Exception('Attempting to redefine variable %s' % (var_name, ))

    if self.memory_limit is not None:
      if self.allocated + size > self.memory_limit:
        raise Exception('Memory limit is exceeded!')

    self.variables[var_name] = self.allocated

    if isinstance(var_value, int):
      var_value = (var_value, )

    elif isinstance(var_value, (list, tuple)):
      pass

    else:
      raise Exception('Value %s is not understood' % (var_value,))

    assert len(var_value) <= size, 'Values exceed variable capacity!'

    self.memory.extend(var_value)

    if len(var_value) < size:
      self.memory.extend((0, ) * (size - len(var_value)))

    self.allocated += size

  def _normalize_argument(self, argument: SExpression):
    value = argument.value
    modifier = argument.modifier
    offset = argument.offset

    if offset is None:
      offset_ = 0

    elif isinstance(offset, Number):
      offset_ = int(offset.value)

    elif isinstance(offset, Identifier):
      if offset.name in self.constants:
        offset_ = self.constants[offset.name]
      else:
        raise ValueError('Unknown constant: %s' % (offset.name, ))

    else:
      raise ValueError('Unknown symbol: %s' % (offset.name,))

    if isinstance(value, Number):
      return type_constant(int(value.value) + offset_, modifier)

    elif isinstance(value, Identifier):
      name = value.name

      if name in self.constants:
        return type_constant(self.constants[name] + offset_, modifier)
      elif name in self.variables:
        return type_variable(self.variables[name] + offset_, modifier)
      else:
        raise Exception('Unknown symbol %s' % (name, ))

    else:
      raise Exception('Unknown argument type!')

  def add_operation(self, operation, arguments):
    if self.memory_limit is not None:
      if self.allocated + len(arguments) + 1 > self.memory_limit:
        raise Exception('Memory limit exceeded!')

    if operation not in OPERATION_GROUPS:
      raise Exception('Unknown operation %s.' % (operation, ))

    if len(arguments) != OPERATION_GROUPS[operation]:
      raise Exception(
        'Operation accepts %s arguments, got %d.' % (OPERATION_GROUPS[operation], len(arguments))
      )

    args, typing = zip(*[ self._normalize_argument(arg) for arg in arguments ])

    opcode = OPCODES_BY_SIGNATURE[operation][typing]
    self.memory.append(opcode)
    self.memory.extend(args)

    self.allocated = len(self.memory)

  def resolve_constant(self, value):
    if isinstance(value, Number):
        return int(value.value)

    elif isinstance(value, Identifier):
      name = value.name

      if name in self.constants:
        return self.constants[name]

      elif name in self.variables:
        raise Exception('%s is a variable, expected a constant.' % (name, ))

      else:
        raise Exception('Unknown symbol %s' % (name, ))

    elif isinstance(value, Array):
      return [
        self.resolve_constant(x)
        for x in value.values
      ]

    else:
      raise Exception('Unknown symbol %s' % (value, ))

  def vm(self, memory_size):
    from ..vm import virtual_machine

    if '__main__' not in self.constants:
      raise Exception('no entry point is specified!')

    if memory_size < len(self.memory):
      raise Exception('size of the code exceeds allocated memory!')

    __main__ = self.constants['__main__']
    return virtual_machine(self.memory, memory_size, __main__)

def asm(code, memory_limit=None, predefined_constants=(), predefined_variables=()):
  executable = Executable(memory_limit=memory_limit)

  for name, value in predefined_constants:
    executable.add_constant(name, value)

  for name, value in predefined_variables:
    executable.add_variable(name, value)

  _, parsed = program(code)

  for entry in parsed:
    try:
      if isinstance(entry, Variable):
        name = entry.identifier.name
        value = 0 if entry.value is None else executable.resolve_constant(entry.value)
        size = 1 if entry.size is None else executable.resolve_constant(entry.size)

        executable.add_variable(name, value, size)

      elif isinstance(entry, Const):
        name = entry.identifier.name
        value = executable.resolve_constant(entry.value)

        executable.add_constant(name, value)

      elif isinstance(entry, Label):
        executable.add_label(entry.identifier.name)

      elif isinstance(entry, Operation):
        op = entry.operation.name
        arguments = entry.arguments
        executable.add_operation(op, arguments)

      elif isinstance(entry, (Comment, Space)):
        pass

      else:
        raise Exception('Unknown line %s' % (entry, ))


    except Exception as e:
      raise Exception('While compiling %s' % (entry, )) from e

  return executable