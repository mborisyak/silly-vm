from collections import namedtuple
import re

__all__ = [
  'expression', 'token', 'either', 'optional', 'seq', 'extract', 'repeat', 'error'
]

class ReExpression(object):
  def __init__(self, name, expression):
    self.expression = re.compile(expression)
    self.name = name

  def __call__(self, s, pos=0, endpos=None):
    if endpos is not None:
      match = self.expression.match(s, pos=pos, endpos=endpos)
    else:
      match = self.expression.match(s, pos=pos)

    if match is None:
      return pos, None

    start, stop = match.span()
    return stop, s[start:stop]

def expression(name):
  def f(expression):
    return ReExpression(name, expression)

  return f

class Token(object):
  def __init__(self, token_class: type, **kwargs):
    self.tokens = {
      name : t if callable(t) else ReExpression(name, t)
      for name, t in kwargs.items()
    }

    self.token_class = token_class

  def __call__(self, s, pos=0, endpos=None):
    results = dict()
    end_token = pos

    for name in self.tokens:
      token = self.tokens[name]
      end_token, result = token(s, pos=end_token, endpos=endpos)

      if result is None:
        return pos, None

      if isinstance(result, Maybe):
        result = result.value

      if isinstance(result, dict):
        if name in result:
          result = result[name]
        else:
          return pos, None

      if not name.startswith('_'):
        results[name] = result

    return end_token, self.token_class(**results)

def token(name):
  def f(**kwargs):
    clazz = namedtuple(name, [key for key in kwargs.keys() if not key.startswith('_') ])
    return clazz, Token(clazz, **kwargs)

  return f

def seq(**kwargs):
  return Token(dict, **kwargs)

def extract(**kwargs):
  main_keys = [ key for key in kwargs if not key.startswith('_') ]
  assert len(main_keys) == 1, 'unambiguous definition'
  main_key = main_keys[0]

  def f(**kwargs):
    return kwargs[main_key]

  return Token(f, **kwargs)

class Either(object):
  def __init__(self, *args):
    self.alternatives = tuple(
      ReExpression(None, arg) if isinstance(arg, str) else arg
      for arg in args
    )

  def __call__(self, s, pos=0, endpos=None):
    for token in self.alternatives:
      end, result = token(s, pos=pos, endpos=endpos)

      if result is not None:
        return end, result

    return pos, None

either = Either

Maybe = namedtuple('Maybe', field_names=['value'])

class Optional(object):
  def __init__(self, token):
    self.token = token

  def __call__(self, s, pos=0, endpos=None):
    end, result = self.token(s, pos=pos, endpos=endpos)
    return end, Maybe(result)

optional = Optional

class Repeat(object):
  def __init__(self, token, sep=None):
    self.token = token
    self.sep = ReExpression('separator', sep) if isinstance(sep, str) else sep

  def __call__(self, s, pos=0, endpos=None):
    results = list()
    endpos = len(s) if endpos is None else endpos

    while pos < endpos:
      pos, result = self.token(s, pos, endpos)
      if result is None:
        return pos, results

      results.append(result)

      if self.sep is not None:
        pos, result = self.sep(s, pos, endpos)
        if result is None:
          return pos, results

    return pos, results

repeat = Repeat

class Error(object):
  def __init__(self, message, limit=32):
    self.message = message
    self.limit = limit

  def __call__(self, s, pos=0, endpos=None):
    start = max(pos - self.limit, 0)
    pre = s[start:pos]
    end = pos + self.limit
    post = s[pos:end]
    raise ValueError('%d: syntax error, %s:\n%s^%s' % (pos, self.message, pre, post))

error = Error