from .meta import *

Space, space = token('space')(space='[\\t ]*')

Comment, comment = token('comment')(
  _space=space,
  text=r'#[^\n]*'
)

Identifier, identifier = token('identifier')(name=r'[_A-Za-z]([A-Za-z_0-9])*')
Number, number = token('number')(value=r'[-]?\d+')
value = either(identifier, number)

SExpression, sexpression = token('SExpression')(
  modifier='[&*]?',
  _space=space,
  value=either(identifier, number),
  offset=optional(seq(
    _space=space,
    _openning='\\[',
    offset=value,
    _closing='\\]'
  ))
)

argument = either(sexpression, number)

Array, array = token('Array')(
  _openning='[{]',
  values=repeat(argument, sep='[\\t ]*,[\\t ]*'),
  _ending='[}]',
)

Const, const = token('Const')(
  _keyword='const',
  _space1=space,
  identifier=identifier,
  _space2=space,
  _eq='=',
  _space3=space,
  value=either(identifier, number)
)

Variable, variable = token('Variable')(
  _keyword='var',
  _space1=space,
  identifier=identifier,
  size=optional(
    seq(
      _space1=space,
      _open=r'[\[]',
      _space2=space,
      size=either(identifier, number),
      _space3=space,
      _close=r'[\]]'
    )
  ),
  value=optional(
    seq(
      _space1=space,
      _eq='=',
      _space2=space,
      value=either(number, identifier)
    )
  ),
)

Label, label = token('Label')(
  _keyword='def',
  _space1=space,
  identifier=identifier,
  _space2=space,
  _closing=':'
)

Operation, operation = token('Operation')(
  operation=identifier,
  _space1=space,
  arguments=repeat(argument, sep=space)
)

line = extract(
  _indentation = space,
  value=either(
    comment,
    const,
    variable,
    label,
    operation,
    space,
    error('unknown expression')
  ),
  _newline=either(
    r'[\t ]*(?:\n|$)',
    error('expected end of the line')
  )
)

program = repeat(line)