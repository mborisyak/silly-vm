from cpython cimport array

cdef extern from "byte_code.h" nogil:
  cdef const int NUMBER_OF_OPERATIONS

  ctypedef struct Instruction:
    unsigned short operation
    int argument1
    int argument2

  cpdef int ARGUMENT_CONST
  cpdef int ARGUMENT_VARIABLE
  cpdef int ARGUMENT_POINTER

  ctypedef struct OperationSignature:
    char * name
    unsigned short group
    unsigned short * arguments

  cdef const OperationSignature operation_signatures[NUMBER_OF_OPERATIONS]

  cdef const int NUMBER_OF_OPERATION_GROUPS

  ctypedef struct OperationGroup:
    int size
    char * name
    unsigned short number_of_arguments

  cdef const OperationGroup operation_groups[NUMBER_OF_OPERATION_GROUPS]


  cdef int NUMBER_OF_STATUSES

  ctypedef struct ReturnStatus:
    int status
    int value
    int instruction_counter

  ctypedef struct PartialReturnStatus:
    int status
    int value
    int instruction_counter
    int iterations_left

  cdef const char * status_names[]

  cpdef int STATUS_OK
  cpdef int STATUS_NOT_FINISHED
  cpdef int STATUS_UNKNOWN_COMMAND
  cpdef int STATUS_MEMORY_OUT_OF_BOUNDS
  cpdef int STATUS_PROGRAM_COUNTER_OUT_OF_BOUNDS
  cpdef int STATUS_ARITHMETIC_ERROR


  cdef ReturnStatus execute(int *, int, int)
  cdef PartialReturnStatus execute_partial(int *, int, int, int)

cdef class VirtualMachine:
  cdef int * memory
  cdef int memory_size

  cdef int instruction_counter
  cdef int __main__

  cpdef void reset(self, )
  cdef ReturnStatus fast_execute(self) nogil
  cdef PartialReturnStatus fast_execute_partial(self, int limit) nogil

  cdef int memory_at(self, int item)

  cpdef int get_instruction_counter(self)
  cpdef void set_instruction_counter(self, int instruction_counter)

cpdef VirtualMachine virtual_machine(array.array memory, int memory_size, int __main__)