from libc.stdlib cimport malloc, free
from cpython cimport array

import array
import enum

from .vm cimport (
  operation_signatures,
  operation_groups,
  NUMBER_OF_OPERATION_GROUPS
)

from .vm cimport (
  STATUS_OK,
  STATUS_NOT_FINISHED,
  STATUS_UNKNOWN_COMMAND,
  STATUS_MEMORY_OUT_OF_BOUNDS,
  STATUS_PROGRAM_COUNTER_OUT_OF_BOUNDS,
  STATUS_ARITHMETIC_ERROR
)


from .vm cimport (
  ARGUMENT_CONST as _ARGUMENT_CONST,
  ARGUMENT_VARIABLE as _ARGUMENT_VARIABLE,
  ARGUMENT_POINTER as _ARGUMENT_POINTER
)

class Typing(enum.Enum):
  CONST = _ARGUMENT_CONST
  VARIABLE = _ARGUMENT_VARIABLE
  POINTER = _ARGUMENT_POINTER

class Status(enum.Enum):
  OK = STATUS_OK
  NOT_FINISHED = STATUS_NOT_FINISHED
  UNKNOWN_COMMAND = STATUS_UNKNOWN_COMMAND
  MEMORY_OUT_OF_BOUNDS = STATUS_MEMORY_OUT_OF_BOUNDS
  PROGRAM_COUNTER_OUT_OF_BOUNDS = STATUS_PROGRAM_COUNTER_OUT_OF_BOUNDS
  ARITHMETIC_ERROR = STATUS_ARITHMETIC_ERROR

OPERATION_GROUPS = dict()
for i in range(NUMBER_OF_OPERATION_GROUPS):
  name = str(operation_groups[i].name, 'utf-8')
  OPERATION_GROUPS[name] = operation_groups[i].number_of_arguments

OPCODES_BY_NAME = dict()
for i in range(NUMBER_OF_OPERATIONS):
  group = operation_signatures[i].group
  OPCODES_BY_NAME[str(operation_groups[group].name, 'utf-8')] = i

OPCODES_BY_SIGNATURE = dict()
for i in range(NUMBER_OF_OPERATIONS):
  group = operation_signatures[i].group
  group_name = str(operation_groups[group].name, 'utf-8')
  if group_name not in OPCODES_BY_SIGNATURE:
    OPCODES_BY_SIGNATURE[group_name] = dict()

  signature = tuple(
    Typing(operation_signatures[i].arguments[j])
    for j in range(operation_groups[group].number_of_arguments)
  )

  OPCODES_BY_SIGNATURE[group_name][signature] = i

cpdef VirtualMachine virtual_machine(array.array memory, int memory_size, int __main__):
  cdef int i
  cdef int j = 0

  vm_memory = <int *> malloc(sizeof(int) * memory_size)
  for i in range(memory_size):
    vm_memory[i] = <int> 0

  for i in range(len(memory)):
    vm_memory[i] = <int> memory[i]

  cdef VirtualMachine vm = VirtualMachine()

  vm.memory = vm_memory
  vm.memory_size = memory_size
  vm.__main__ = __main__
  vm.instruction_counter = __main__

  return vm

cdef class VirtualMachine:
  def __cinit__(self):
    self.memory = NULL
    self.memory_size = 0

    self.__main__ = 0
    self.instruction_counter = 0

  def __dealloc__(self):
    if self.memory != NULL:
      free(self.memory)

  def __call__(self, limit=None):
    if limit is None:
      return self.execute()
    else:
      return self.execute_partial(limit)

  cpdef void reset(self, ):
    cdef int i
    for i in range(self.memory_size):
      self.memory[i] = 0

    self.instruction_counter = self.__main__

  cdef ReturnStatus fast_execute(self) nogil:
    cdef ReturnStatus return_status = execute(
      self.memory, self.memory_size,
      self.instruction_counter
    )
    self.instruction_counter = return_status.instruction_counter

    return return_status

  cdef PartialReturnStatus fast_execute_partial(self, int limit) nogil:
    cdef PartialReturnStatus return_status = execute_partial(
      self.memory, self.memory_size,
      self.instruction_counter,
      limit,
    )
    self.instruction_counter = return_status.instruction_counter

    return return_status


  def execute(self):
    cdef ReturnStatus return_status = self.fast_execute()
    return return_status.status, return_status.value

  def execute_partial(self, int limit):
    cdef PartialReturnStatus return_status = self.fast_execute_partial(limit)
    return return_status.status, return_status.value, return_status.iterations_left

  cpdef int get_instruction_counter(self):
    return self.instruction_counter

  cpdef void set_instruction_counter(self, int instruction_counter):
    self.instruction_counter = instruction_counter

  def memory_dump(self):
    cdef array.array dump = array.array('i')
    array.resize(dump, self.memory_size)
    cdef int i

    for i in range(self.memory_size):
      dump[i] = self.memory[i]

    return dump

  cdef int memory_at(self, int item):
    return self.memory[item]

  def __getitem__(self, int item):
    return self.memory_at(item)

  def __setitem__(self, int key, int value):
    if key >= self.memory_size or key < 0:
      raise IndexError('invalid vm_memory location')

    self.memory[key] = value
