#include <stdlib.h>

#include "byte_code.h"

$operation_codes

$operation_arguments

const OperationSignature operation_signatures[NUMBER_OF_OPERATIONS] = $operation_signatures;

const OperationGroup operation_groups[NUMBER_OF_OPERATION_GROUPS] = $operation_groups;

const char * status_names[NUMBER_OF_STATUSES] = {
  "ok",
  "not finished",
  "unknown operation",
  "memory out of bound",
  "program counter out of bounds",
  "arithmetic error"
};

#define ADVANCE_COUNTER() instruction_counter++;
#define SET_COUNTER(X) instruction_counter = X;

#define EXTERNAL_RETURN(X, Y) (ReturnStatus){\
  .status = X,\
  .value = Y,\
};

#define UNKNOWN_COMMAND_RETURN() (ReturnStatus){\
  .status = STATUS_UNKNOWN_COMMAND,\
  .value = operation,\
};

#define CHECK_MEMORY(X) if (X < 0 || X >= memory_size) {\
  return (ReturnStatus){\
    .status = STATUS_MEMORY_OUT_OF_BOUNDS,\
    .value = X,\
    .instruction_counter = instruction_counter\
  };\
};

#define CHECK_INSTRUCTION_COUNTER(X) if (X < 0 || X >= memory_size) {\
  return (ReturnStatus){\
    .status = STATUS_PROGRAM_COUNTER_OUT_OF_BOUNDS,\
    .value = X,\
    .instruction_counter = instruction_counter\
  };\
};

#define CHECK_ARGUMENTS(N) if (instruction_counter + N >= memory_size) {\
  return (ReturnStatus){\
    .status = STATUS_PROGRAM_COUNTER_OUT_OF_BOUNDS,\
    .value = instruction_counter,\
    .instruction_counter = instruction_counter\
  };\
};

ReturnStatus execute(
  int * memory, int memory_size,
  int instruction_counter
) {
  while (instruction_counter < memory_size) {
    const int operation = memory[instruction_counter++];

    switch (operation) {
      $switch_statement
    }
  }

  return (ReturnStatus){
    .status = STATUS_PROGRAM_COUNTER_OUT_OF_BOUNDS,
    .value = instruction_counter,
    .instruction_counter = instruction_counter
  };
}

#undef EXTERNAL_RETURN
#undef UNKNOWN_COMMAND_RETURN
#undef CHECK_MEMORY
#undef CHECK_INSTRUCTION_COUNTER
#undef CHECK_ARGUMENTS

#define EXTERNAL_RETURN(X, Y) (PartialReturnStatus){\
  .status = X,\
  .value = Y,\
  .iterations_left = iterations_left,\
  .instruction_counter = instruction_counter\
};
#define UNKNOWN_COMMAND_RETURN() (PartialReturnStatus){\
  .status = STATUS_UNKNOWN_COMMAND,\
  .value = operation,\
  .iterations_left = iterations_left,\
  .instruction_counter = instruction_counter\
};

#define CHECK_MEMORY(X) if (X < 0 || X >= memory_size) {\
  return (PartialReturnStatus){\
    .status = STATUS_MEMORY_OUT_OF_BOUNDS,\
    .value = X,\
    .iterations_left = iterations_left,\
    .instruction_counter = instruction_counter\
  };\
};

#define CHECK_INSTRUCTION_COUNTER(X) if (X < 0 || X >= memory_size) {\
  return (PartialReturnStatus){\
    .status = STATUS_PROGRAM_COUNTER_OUT_OF_BOUNDS,\
    .value = X,\
    .iterations_left = iterations_left,\
    .instruction_counter = instruction_counter\
  };\
};

#define CHECK_ARGUMENTS(N) if (instruction_counter + N >= memory_size) {\
  return (PartialReturnStatus){\
    .status = STATUS_PROGRAM_COUNTER_OUT_OF_BOUNDS,\
    .value = instruction_counter,\
    .iterations_left = iterations_left,\
    .instruction_counter = instruction_counter\
  };\
};

PartialReturnStatus execute_partial(
  int * memory, int memory_size,
  int instruction_counter,
  int limit
) {
  for (int iterations_left = limit; iterations_left > 0; --iterations_left) {
    if (instruction_counter >= memory_size) {
      return (PartialReturnStatus){
        .status = STATUS_PROGRAM_COUNTER_OUT_OF_BOUNDS,
        .value = instruction_counter,
        .iterations_left = iterations_left,
        .instruction_counter = instruction_counter
      };
    }

    const int operation = memory[instruction_counter++];

    switch (operation) {
      $switch_statement
    }
  }

  return (PartialReturnStatus){
    .status = STATUS_NOT_FINISHED,
    .value = instruction_counter,
    .iterations_left = 0,
    .instruction_counter = instruction_counter,
  };
}

#undef EXTERNAL_RETURN
#undef UNKNOWN_COMMAND_RETURN
#undef CHECK_MEMORY
#undef CHECK_INSTRUCTION_COUNTER
#undef CHECK_ARGUMENTS