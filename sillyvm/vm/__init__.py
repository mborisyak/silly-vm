from .vm import VirtualMachine, virtual_machine
from .vm import Typing, Status

from .vm import OPERATION_GROUPS
from .vm import OPCODES_BY_NAME, OPCODES_BY_SIGNATURE