#ifndef SILLY_VM__BYTE_CODE_H
#define SILLY_VM__BYTE_CODE_H

#define NUMBER_OF_OPERATIONS $number_of_operations
extern const char * operation_names[NUMBER_OF_OPERATIONS];

#define ARGUMENT_CONST 0
#define ARGUMENT_VARIABLE 1
#define ARGUMENT_POINTER 2

typedef struct {
  char * name;
  unsigned short group;
  unsigned short const * arguments;
} OperationSignature;

extern const OperationSignature operation_signatures[NUMBER_OF_OPERATIONS];

#define NUMBER_OF_OPERATION_GROUPS $number_of_operation_groups

typedef struct {
  int size;
  char * name;
  unsigned short number_of_arguments;
} OperationGroup;

extern const OperationGroup operation_groups[NUMBER_OF_OPERATION_GROUPS];

typedef struct {
  int status;
  int value;
  int instruction_counter;
} ReturnStatus;

typedef struct {
  int status;
  int value;
  int instruction_counter;
  int iterations_left;
} PartialReturnStatus;

#define NUMBER_OF_STATUSES 6

#define STATUS_OK 0
#define STATUS_NOT_FINISHED -1
#define STATUS_UNKNOWN_COMMAND -2
#define STATUS_MEMORY_OUT_OF_BOUNDS -3
#define STATUS_PROGRAM_COUNTER_OUT_OF_BOUNDS -4
#define STATUS_ARITHMETIC_ERROR -5

extern const char * status_names[NUMBER_OF_STATUSES];

ReturnStatus execute(
  int * memory, int memory_size,
  int instruction_counter
);

PartialReturnStatus execute_partial(
  int * memory, int memory_size,
  int instruction_counter,
  int limit
);

#endif //SILLY_VM__BYTE_CODE_H
