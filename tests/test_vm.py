from sillyvm import *

def available_programs():
  import os
  here = os.path.dirname(__file__)
  return [
    item
    for item in os.listdir(os.path.join(here, 'programs'))
    if not item.endswith('.c')
  ]

def read_source(filename):
  import os
  here = os.path.dirname(__file__)

  with open(os.path.join(here, 'programs', filename), 'r') as f:
    return f.read()


units = ['sec', 'millisec', 'microsec', 'nanosec']
def format_time(time_sec):
  for i in range(len(units)):
    if time_sec < 1:
      time_sec *= 1000
    else:
      return '%.3lf %s' % (time_sec, units[i])

  return '%.3lf %s' % (time_sec, units[-1])

def timeit(callable):
  import time

  start_time = time.time()
  result = callable()
  delta = time.time() - start_time

  return delta, result

def eval_vm(vm, execute):
  vm.reset()
  delta, result = timeit(execute)

  memory_dump = vm.memory_dump()
  status = result[0]

  print('  exit status: %s (%s)' % (Status(status), ', '.join(str(v) for v in result[1:])))
  print('  time elapsed: %s' % (format_time(delta),))

  print('  vm_memory:')
  for i in range(10):
    if memory_dump[i] != 0:
      print('    %04d: %d' % (i, memory_dump[i]))

def test_assembler():
  for program in available_programs():
    asm(read_source(program))

def full_partial(vm, step_size=32):
  while True:
    status, value, _ = vm.execute_partial(step_size)
    if status == -1 and status != 0:
      vm.set_instruction_counter(value)
    elif status == 0:
      return status, value
    else:
      raise Exception('unexpected return status %s' % (Status(status), ))

def test_vm():
  print()

  for program in available_programs():
    print()
    prog = asm(read_source(program))
    print(program)

    print('program: %s' % (program,))
    vm = prog.vm(1024)
    eval_vm(vm, vm.execute)

    print('program: %s (partial)' % (program,))
    vm = prog.vm(1024)
    eval_vm(vm, lambda : vm.execute_partial(32))

    print('program: %s (full partial)' % (program,))
    vm = prog.vm(1024)
    eval_vm(prog.vm(1024), lambda: full_partial(vm, 32))

def test_statuses():
  print()
  for status in Status:
    print(status, status.value)