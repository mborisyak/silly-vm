from sillyvm.asm import asm
N = 100000000

def get_baseline(source, target, flags):
  import subprocess as sp
  import os
  import time

  sp.check_call(['gcc', source] + flags + ['-o', target])
  st = time.time()
  sp.check_call([target])
  et = time.time()

  os.remove(target)

  return (et - st) * (10 ** 9) / N

def test_grammar():
  import os

  current_dir = os.path.dirname(__file__)
  with open(os.path.join(current_dir, 'programs', 'speed_test')) as f:
    code = f.read()

  executable = asm(code)
  print(executable.memory)
  print(executable.variables)
  print(executable.constants)

  vm = executable.vm(1024)

  import time

  st = time.time()
  result = vm.execute()
  et = time.time()

  time_execute = (et - st) * (10 ** 9) / N

  print('Execute: %.3lf nsec/it' % time_execute)

  vm = executable.vm(1024)

  st = time.time()
  result = vm.execute_partial(int(2 * 10 ** 9))
  et = time.time()

  time_partial = (et - st) * (10 ** 9) / N

  print('Partial execute: %.3lf nsec/it' % time_partial)

  import os
  source = os.path.join(current_dir, 'programs', 'speed_test.c')
  target = os.path.join(current_dir, 'programs', 'baseline')

  time_baseline_O0 = get_baseline(source, target, ['-O0'])
  print(
    'Baseline -O0: %.3lf nsec/it, slowdown %.1lfx' % (
      time_baseline_O0,
      time_execute / time_baseline_O0
    )
  )

  time_baseline_O1 = get_baseline(source, target, ['-O1'])
  print(
    'Baseline -O1: %.3lf nsec/it, slowdown %.1lfx' % (
      time_baseline_O1,
      time_execute / time_baseline_O1
    )
  )

  time_baseline_O2 = get_baseline(source, target, ['-O2'])
  print(
    'Baseline -O2: %.3lf nsec/it, slowdown %.1lfx' % (
      time_baseline_O2,
      time_execute / time_baseline_O2
    )
  )

  time_baseline_Ofast = get_baseline(source, target, ['-Ofast'])
  print(
    'Baseline -Ofast: %.3lf nsec/it, slowdown %.1lfx' % (
      time_baseline_Ofast,
      time_execute / time_baseline_Ofast
    )
  )

if __name__ == '__main__':
  test_grammar()