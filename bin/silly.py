#!/usr/bin/env python

def zeros(memory):
  for i in range(len(memory) - 1, -1, -1):
    if memory[i] != 0:
      return i + 1

  return 0

def silly():
  import sys

  if len(sys.argv) < 2:
    print('Usage: {prog} <path to an asm code>')
    return

  path = sys.argv[1]
  with open(path, 'r') as f:
    code = f.read()

  from sillyvm import asm
  vm = asm(code).vm(memory_size=1024)
  vm.execute()

  memory = vm.memory_dump()
  nonzero_length = zeros(memory)

  import math
  max_linenumber_lenght = int(math.ceil(math.log10(nonzero_length)))
  line_format = '%{size}d %d'.format(size=max_linenumber_lenght)

  for i in range(nonzero_length):
    print(line_format % (i, memory[i]))

if __name__ == '__main__':
  silly()
