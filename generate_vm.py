CONST = 'const'
VAR = 'var'
PTR = 'ptr'

macro_types = {
  CONST : 'ARGUMENT_CONST',
  VAR : 'ARGUMENT_VARIABLE',
  PTR : 'ARGUMENT_POINTER',
}

def check_number_of_arguments(num: int):
  return 'CHECK_ARGUMENTS(%d);' % (num - 1, )

def unpack_argument(typing, id: int, last=False):
  instruction_counter = 'instruction_counter++' if not last else 'instruction_counter'

  if typing == CONST:
    const = 'const_%d' % (id, )
    return const, \
           'const int {const} = memory[{instruction_counter}];'.format(
             const=const,
             instruction_counter=instruction_counter
           )

  elif typing == VAR:
    var = 'var_%d' % (id, )
    return 'memory[{var}]'.format(var=var), \
           'const int {var} = memory[{instruction_counter}];\n' \
           'CHECK_MEMORY({var});'.format(
             var=var,
             instruction_counter=instruction_counter
           )

  elif typing == PTR:
    ptr = 'ptr_%d' % (id, )
    var = 'var_%d' % (id, )
    return 'memory[{var}]'.format(var=var), \
           'const int {ptr} = memory[{instruction_counter}];\n' \
           'CHECK_MEMORY({ptr});\n' \
           'const int {var} = memory[{ptr}];\n' \
           'CHECK_MEMORY({var});'.format(
             var=var,
             ptr=ptr,
             instruction_counter=instruction_counter
           )

  else:
    raise ValueError('Typing is not understood')

def external(x: [CONST, VAR, PTR], y: [CONST, VAR, PTR]):
  return 'ADVANCE_COUNTER();\n' \
         'return EXTERNAL_RETURN(%s, %s);' % (x, y)

def goto(x: [CONST, VAR, PTR]):
  return 'CHECK_INSTRUCTION_COUNTER(%s);\n' \
         'SET_COUNTER(%s);' % (x, x)

def jump(x: [VAR, PTR], y: [CONST, VAR, PTR]):
  return 'if (%s > 0) {\n' \
         '  CHECK_INSTRUCTION_COUNTER(%s);\n' \
         '  SET_COUNTER(%s);\n' \
         '} else {\n' \
         '  ADVANCE_COUNTER();\n' \
         '}' % (x, y, y)

def assign(x: [VAR, PTR], y: [CONST, VAR, PTR]):
  return '%s = %s;\n' \
         'ADVANCE_COUNTER();' % (x, y)

def add(x: [VAR, PTR], y: [CONST, VAR, PTR]):
  return '%s += %s;\n' \
         'ADVANCE_COUNTER()' % (x, y)

def sub(x: [VAR, PTR], y: [CONST, VAR, PTR]):
  return '%s -= %s;\n' \
         'ADVANCE_COUNTER();' % (x, y)

def mul(x: [VAR, PTR], y: [CONST, VAR, PTR]):
  return '%s *= %s;\n' \
         'ADVANCE_COUNTER();' % (x, y)

def div(x: [VAR, PTR], y: [CONST, VAR, PTR]):
  return '%s /= %s;\n' \
         'ADVANCE_COUNTER();' % (x, y)

def mod(x: [VAR, PTR], y: [CONST, VAR, PTR]):
  return '%s %%= %s;\n' \
         'ADVANCE_COUNTER();' % (x, y)

def random(x: [VAR, PTR], y: [CONST, VAR, PTR]):
  return '%s = (rand() %% (%s));\n' \
         'ADVANCE_COUNTER();' % (x, y)

def generate_vm(operations, debug=False):
  def indent(string, indent):
    ind = ' ' * indent
    return ('\n%s' % (ind,)).join(string.split('\n'))

  import inspect
  from itertools import product

  operation_groups = dict()
  number_of_arguments = dict()

  for operation in operations:
    operation_name = operation.__name__
    operation_groups[operation_name] = list()

    signature = inspect.signature(operation)
    allowed_types = [
      signature.parameters[param].annotation
      for param in signature.parameters
    ]
    number_of_arguments[operation_name] = len(signature.parameters)

    for typing in product(*allowed_types):
      definitions = list()
      arguments = list()

      typed_name = '%s_%s' % (operation_name, '_'.join(typing))

      definitions.append(check_number_of_arguments(len(typing)))

      for i, arg_type in enumerate(typing):
        arg_name, arg_def = unpack_argument(arg_type, i, i == len(typing) - 1)
        definitions.append(arg_def)
        arguments.append(arg_name)


      if debug:
        code = '%s\n\n%s\n%s' % (
          '\n'.join(definitions),
          'printf("%%s %s\\n", operation_signatures[operation].name, %s);' % (
            ' '.join(['%d' for _ in arguments]),
            ', '.join(arguments)
          ),
          operation(*arguments)
        )
      else:
        code = '%s\n\n%s' % ('\n'.join(definitions), operation(*arguments))
      operation_groups[operation_name].append((typed_name, typing, code))

  switch_statement = '\n'.join(
    [
      'case %s: {\n  %s\n}\nbreak;\n' % (
        name.upper(),
        indent(code, 2)
      )
      for group in operation_groups
      for name, _, code in operation_groups[group]
    ] + [
      'default:\n  ADVANCE_COUNTER();\n  return UNKNOWN_COMMAND_RETURN();'
    ]
  )

  operation_codes = '\n'.join([
    '#define %s %i' % (name.upper(), i)
    for i, name in enumerate([
      name
      for group in operation_groups
      for name, _, _ in operation_groups[group]
    ])
  ])

  operation_arguments = '\n'.join([
    'const unsigned short {name}_arguments[] = {{ {args} }};'.format(
      name=typed_name,
      args=', '.join([macro_types[t] for t in typing])
    )
    for group in operation_groups
    for typed_name, typing, _ in operation_groups[group]
  ])

  operation_signatures = '{\n%s\n}' % (
    ',\n'.join([
      '  (OperationSignature){{\n'
      '    .name = "{name}",\n'
      '    .group = {group},\n'
      '    .arguments = {name}_arguments\n'
      '  }}'.format(
          name=typed_name,
          group=group_i,
          num_args=len(typing),
        )
      for group_i, group in enumerate(operation_groups)
      for typed_name, typing, _ in operation_groups[group]
    ])
  )

  groups = list()
  for group in operation_groups:
    groups.append('  (OperationGroup){ .size = %d, .number_of_arguments = %d, .name = "%s" }' % (
      len(operation_groups[group]), number_of_arguments[group], group
    ))

  groups = '{\n%s\n}' % (',\n'.join(groups))

  return dict(
    switch_statement=indent(switch_statement, 6),
    operation_codes=operation_codes,
    operation_arguments=operation_arguments,
    operation_signatures=operation_signatures,
    number_of_operations=sum([ len(operation_groups[group]) for group in operation_groups ]),
    operation_groups=groups,
    number_of_operation_groups=len(operation_groups)
  )

if __name__ == '__main__':
  import os
  import string
  import sys

  operations = [
    external,
    goto,
    jump,
    assign,
    add,
    sub,
    mul,
    div,
    mod,
    random,
  ]

  if len(sys.argv) == 2:
    if sys.argv[1] == 'debug':
      debug = True
    else:
      raise Exception('Unknown argument %s' % (sys.argv[1], ))
  else:
    debug = False

  generated = generate_vm(operations, debug=debug)

  here = os.path.dirname(__file__)

  with open(os.path.join(here, 'sillyvm', 'vm', 'byte_code.template.c'), 'r') as f:
    template = string.Template(f.read())

  with open(os.path.join(here, 'sillyvm', 'vm', 'byte_code.c'), 'w') as f:
    f.write(
      template.safe_substitute(**generated)
    )

  with open(os.path.join(here, 'sillyvm', 'vm', 'byte_code.template.h'), 'r') as f:
    template = string.Template(f.read())

  with open(os.path.join(here, 'sillyvm', 'vm', 'byte_code.h'), 'w') as f:
    f.write(
      template.safe_substitute(**generated)
    )